package ru.ncedu.golovneva.bc;

import java.io.*;

/**
 * This is the main class.
 * The file name is input as a command line argument.
 * The file name must be with a path.
 * It is assumed that the class extending the ListResourceBundle compiles without errors.
 * @autor Golovneva Maria
 */
public class Main {
    public static void main(String[] args) throws IOException {
        BundleConverter bc = new BundleConverter(args[0]);
        bc.Convert();
    }
}
