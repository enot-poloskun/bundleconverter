package ru.ncedu.golovneva.bc;

import java.io.*;

public class BundleConverter {

    private String filename;

    public BundleConverter(String filename) {

        this.filename = filename;
    }

    /**
     * Reads a line by line file {@link BundleConverter#filename}.
     * Converts and writes to the property file by the {@link BundleWriter#write(String)} a line that matches a regex.
     * @throws IOException
     */

    public void Convert() throws IOException {

        String regex = ".*\\{\".*\",.*\".*\"\\}.*"; //string like {"a", "b"},

        File source_file = new File(filename);

        String property_filename = source_file.getName().replaceAll("java", "properties");
        FileOutputStream property_file = new FileOutputStream(property_filename);

        FileReader fr = new FileReader(source_file);
        BufferedReader reader = new BufferedReader(fr);

        BundleWriter bw = new StandardBundleWriterWithComments(property_file);

        String line = reader.readLine();
        while (line != null) {
            if (line.matches(regex)) {
                bw.write(line);
            }
            line = reader.readLine();
        }
        property_file.flush();
        property_file.close();
    }
}
