package ru.ncedu.golovneva.bc;

import java.io.FileOutputStream;
import java.io.IOException;

abstract public class BundleWriter {

    private FileOutputStream fileOutputStream;

    public BundleWriter(FileOutputStream fileOutputStream) {
        this.fileOutputStream = fileOutputStream;
    }

    /**
     * Builds from the values ​​a string suitable for writing to a property file.
     * @param values
     * @return converted line
     */
    abstract public String buildString(String[] values);

    /**
     * Splits a line into values for conversion.
     * Writes the converted line to a property file.
     * @param line
     * @throws IOException
     */
    public void write(String line) throws IOException {
        String[] values = line.split("\"|//");
        String string = buildString(values);
        fileOutputStream.write(string.getBytes());
    }
}

/**
 * This class is for writing to the property file a string in the form of "key=value".
 */
class StandardBundleWriter extends BundleWriter {

    public StandardBundleWriter(FileOutputStream fileOutputStream) {
        super(fileOutputStream);
    }


    @Override
    public String buildString(String[] values) {
        return  values[1] + "=" + values[3] + "\n";
    }
}

/**
 * This class is for writing to the property file a string in the form of
 *
 * # comment (if exists)
 * key=value
 */
class StandardBundleWriterWithComments extends BundleWriter{

    public StandardBundleWriterWithComments(FileOutputStream fileOutputStream) {
        super(fileOutputStream);
    }

    @Override
    public String buildString(String[] values) {
        String result = "";
        if (values.length > 5)
            result += "# " + values[5] + "\n";
        result += values[1] + "=" + values[3] + "\n";
        return result;
    }
}

/**
 * This class is for writing to the property file a string in the form of "key=value"
 * where the value characters are in the format "\ uXXXX".
 * XXXX is the hexadecimal representation of the unicode character.
 */

class UnicodeBundleWriter extends BundleWriter{

    public UnicodeBundleWriter(FileOutputStream fileOutputStream) {
        super(fileOutputStream);
    }

    @Override
    public String buildString(String[] values) {
        String unicode_string = "";
        for (char c : values[3].toCharArray()) {
            int code = (int)c;
            unicode_string += "\\u" + String.format("%04X", code);
        }
        return values[1] + "=" + unicode_string + "\n";
    }
}

/**
 * This class is for writing to the property file a string in the form of
 *
 * # comment (if exists)
 * key=value
 *
 * where the value characters are in the format "\ uXXXX"
 * XXXX is the hexadecimal representation of the unicode character
 */

class UnicodeBundleWriterWithComments extends BundleWriter{

    public UnicodeBundleWriterWithComments(FileOutputStream fileOutputStream) {
        super(fileOutputStream);
    }

    @Override
    public String buildString(String[] values) {
        String result = "";
        String unicode_string = "";
        if (values.length > 5)
            result += "# " + values[5] + "\n";
        for (char c : values[3].toCharArray()) {
            int code = (int)c;
            unicode_string += "\\u" + String.format("%04X", code);
        }
        result += values[1] + "=" + unicode_string + "\n";
        return result;
    }
}
