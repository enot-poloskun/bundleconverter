# BundleConverter
**Задача:**

Написать конвертер бандлов (bundles) – файлов, которые в Java используются для хранения наборов локализуемых (русифицируемых, франкофицируемых и т.д.) строк. В bundles обычно хранятся элементы пользовательского интерфейса серьезных программ. Цель bundles – реализация многоязычного пользовательского интерфейса без усложнения кода программ: программисты должны лишь доставать строки независящим от языка вызовом метода ResourceBundle.getBundle(имя_набора).getString(ключ_строки); при этом класс ResourceBundle сам ищет файл, соответствующий языку пользователя (прописанному в настройках ОС).
Два способа хранения строк (в виде «ключ–значение») в Java реализуются двумя подклассами класса java.util.ResourceBundle – ListResourceBundle (строки записываются в исходнике Java-класса, как правило, внутри массива) и PropertyResourceBundle (строки записываются в файле *.properties в формате "ключ=значение").
Смысл задачи: иногда возникают ситуации, когда нужно из ListResourceBundle получить PropertyResourceBundle с тем же содержимым, но делать это вручную – долгая скучная работа.
